<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-smart-interna.php' ?>

<section id="spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="product-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive phone-logo" src="img/spin/spin-logo.png">
            <p class="phone-modelo">sp-50</p>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive spin-phone-interna" src="img/spin/phone-02.png">  
  <div class="container-fluid iconos-producto-cintillo">
    <div class="row">              
      <div class="container">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">8 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla FWVGA IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería 2000 mAh</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="pantalla-spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-size-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">pantalla<br><span>IPS 5"</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-4">
            <p class="interna-cintillo-right-text">TFT de 16 millones de colores, inmensamente mejorado la transmisión de la luz. Una increíble experiencia tan cercana a lo real.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/spin/phone-03.png">             
      </div>
    </div>
  </div>
</section>

<section id="camara-spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cam-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">cámara<br><span>8 Mpx</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-4">
            <p class="interna-cintillo-right-text">Con la cámara principal de 8MP saca las mejores fotos, perfectas y claras. Capturar y compartir tus momentos favoritos nunca ha sido más fácil y divertido.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/spin/phone-04.png">             
      </div>
    </div>
  </div>
</section>

<section id="hd-spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-hd-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">pantalla<br><span>qhd ips</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text">Con una definición de pantalla de 960 x 540 pixeles y tecnología IPS de construcción, hace de este equipo tan exclusivo como único ya que estas características solo las podes ver en equipos de mayo costo.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/spin/phone-05.png">             
      </div>
    </div>
  </div>
</section>

<section id="cell-spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cell-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">procesador<br><span>quad core</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text">MTK6582, Procesador Integrado de Cuatro Núcleos Cortex A7 a 1.3 GHz, te dará el mejor desempeño con el más bajo consumo. Disfruta de un excelente rendimiento gracias a 1GB de memoria RAM. Videos fluidos y juegos de gran potencial gráfico. Un Mundo fantástico que explota en tu pantalla!</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/spin/phone-06.png">             
      </div>
    </div>
  </div>
</section>

<section id="sim-spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/dual-sim-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-blue">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">Dual<br><span>sim</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text-white">El mejor balance entre el trabajo y la vida cotidiana, el uso de Dual Sim es la mejor opción. Que más? El soporte de tarjeta de memoria Micro SD de hasta 64GB que te brindara mucho más espacio de almacenamiento para todas tus fotos, videos y música.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/spin/phone-07.png">             
      </div>
    </div>
  </div>
</section>

<section id="bateria-spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/bat-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-black">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">bateria<br><span>2000<span class="mah">mAh</span></span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text-white">Excelente capacidad, con 2000 mAh de carga que te proporcionaran muchas horas de satisfacción. Para el entretenimiento, el trabajo, o la vida cotidiana, en estas necesidades diarias te ayudaran a solucionar tus problemas. El momento de diversión ya empezó!</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/spin/phone-08.png">             
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <div class="col-md-4">
            <p class="info-section-title">Caracteristicas Basicas</p>
            <p class="info-section-text">
              <span>Sitema Operativo:</span> Android 4.4 (KitKat)<br>
              <span>Dimensiones:</span> 143.4*71.2*7.9mm （L×W×H）<br>
              <span>Peso:</span> 130g<br>
              <span>CPU:</span> 1.3Ghz
            </p>
            <p class="info-section-title">Cámara</p>
            <p class="info-section-text">
              <span>Camara Trasera:</span> 8 Mp<br>
              <span>Camara Frontal:</span> 2 Mp<br>
              <span>Flash:</span> Si
            </p>
            <p class="info-section-title">Pantalla</p>
            <p class="info-section-text">
              <span>Tamaño de Pantalla:</span> 5”<br>
              <span>Calidad de Pantalla:</span> FWVGA IPS LCD<br>
              <span>Resolusion:</span> 854 x 480
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Red</p>
            <p class="info-section-text">
              <span>SIM:</span> WCDMA/GSM Dual SIM Dual Standby<br>
              <span>Frecuencias:</span> WCDMA 850/1900/2100 & GSM 850/900/1800/1900
            </p>  
            <p class="info-section-title">Interfaz</p>
            <p class="info-section-text">
              <span>GPS:</span>Soporta <br>
              <span>Bluetooth:</span> Soporta<br>
              <span>WIFI:</span> Soporta 
            </p> 
            <p class="info-section-title">Bateria</p>
            <p class="info-section-text">
              <span>Capacidad:</span> 2000mAh polymer<br>
            </p> 
            <p class="info-section-title">Sensores</p>
            <p class="info-section-text">
              <span>Sensor de Movimiento:</span> Soporta<br>
              <span>G-Sensor:</span> Soporta
            </p>   
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Multimedia</p>
            <p class="info-section-text">
              <span>Musica:</span> MP3, wav, wma, ogg, ape, flac, aac, etc.<br>
              <span>Video:</span> H.264/263, MPEG4, VP8，MVC, etc.<br>
              <span>Imágenes:</span> JPEG, PNG, GIF, BMP, etc.
            </p> 
            <p class="info-section-title">Memoria</p>
            <p class="info-section-text">
              <span>Memoria Interna:</span> 8GB<br>
              <span>Memoria RAM:</span> 1GB<br>
              <span>Memoria Externa:</span> Micro SD card ( T-FLASH card), max 32GB
            </p>  
            <p class="info-section-title">Camara de Video</p>
            <p class="info-section-text">
              <span>Grabacion de Video:</span> 720P HD 60Frame/s<br>
            </p>  
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>