<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>            
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a class="active-navbar" href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid header-subclass is-visible" data-nav-status="toggle">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-2 kill-padding">
          <h6>VIDEO</h6>
          <ul class="subclass-list">
            <li><a href="dvp-720g.php">DVP-720G</a></li>
            <li><a href="dvp-920g.php">DVP-920G</a></li>
            <li><a href="video.php#vr-101b">VR-101B</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>