<footer>
  <div class="container-fluid contactos-pad">
    <div class="container">
      <div align="center" class="col-md-10 col-md-offset-1"> 
        <img src="img/assets/logo.png">
        <ul class="footer-links">
          <li><a href="">smartphones</a> | </li>
          <li><a href="">tablets</a> | </li>
          <li><a href="">audio</a> | </li>
          <li><a href="">video</a> | </li>
          <li><a href="">car audio</a> | </li>
          <li><a href="">empresa</a> | </li>
          <li><a href="">contacto</a></li>
        </ul>
      </div>         
    </div>       
  </div>
  <div class="container-fluid">
    <div class="row">
      <div align="center">
        <p class="copyright">Copyright © 2016 suzukitime Inc. All rights reserved. Terms of Use Updated Privacy Policy</p>
      </div>
    </div>
  </div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){

    var previousScroll = 0;

    $(window).scroll(function(){

      var currentScroll = $(this).scrollTop();

      if (currentScroll > 0 && currentScroll < $(document).height() - $(window).height()){

        if (currentScroll > previousScroll){
          window.setTimeout(hideNav, 300);
        } 

        else {
          window.setTimeout(showNav, 300);
        }

      previousScroll = currentScroll;

      }

    });

    function hideNav() {
      $("[data-nav-status='toggle']").removeClass("is-visible").addClass("is-hidden");
    }
    function showNav() {
      $("[data-nav-status='toggle']").removeClass("is-hidden").addClass("is-visible");
    }

  });
</script>    
<script src="js/owl.carousel.js"></script>
<script src="js/SmoothScroll.js"></script>
<script src="js/mailer.js"></script>
<script>
   $('.owl-carousel').owlCarousel({
    items:1,
    loop:false,
    center:true,
    margin:10,
    URLhashListener:true,
    autoplayHoverPause:true,
    startPosition: 'URLHash'
});
</script>
<script>
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top -75
          }, 1000);
          return false;
        }
      }
    });
  });
</script>         
<script>
  $('.subclass-list li a').on('click',function(){
    $('.subclass-list li a').removeClass('subclass-active');
    $(this).addClass('subclass-active');
  });
</script>    
</body>
</html>