<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-audio-interna.php' ?>   

<section id="mc-210" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="info-audio-cintillo-interna">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MICROSISTEMA</h2>
            <p class="equipo-modelo">mc-210</p>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive mc210-audio-interna" src="img/mc210/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">CD/MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-10 col-md-offset-1">              
          <div class="col-md-4">
            <p class="info-section-title">Conectividad inalámbrica</p>
            <p class="info-section-text">
              Escucha toda tu música de manera inalámbrica via Bluetooh. Este minisistema de audio te ofrece un sonido con la mejor calidad y prestaciones únicas. 
            </p>
            <p class="info-section-title">Entrada USB</p>
            <p class="info-section-text">
              Reproduci música desde un pen drive o reproductor portátil con formato MP3. 
            </p>
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Entrada Auxiliar de Audio</p>
            <p class="info-section-text">
              Esta conexión te permite escuchar la música de cualquier dispositivo portátil. 
            </p>  
            <p class="info-section-title">Radio FM y lector de CD</p>
            <p class="info-section-text">
              Podrás sintonizar emisoras de FM en forma digital o simplemente escuchar tu CD favorito.  
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Diseño compacto</p>
            <p class="info-section-text">
              Su moderno diseño y tamaño reducido, hace de este minisistema de audio un producto ideal para cualquier ambiente del hogar manteniendo la fidelidad y calidad de sonido. 
            </p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>