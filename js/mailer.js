! function() {
    "use strict";

    function c() {
        var e = $("#contact-form"),
            n = $("#form-messages");
        $(n).slideUp(), $(e).submit(function(a) {
            a.preventDefault();
            var t = $(e).serialize();
            $("#name").val() && $("#email").val() && $("#message").val() ? $("#form-messages").text("Enviando. Favor espere...").slideDown() : $("#form-messages").text("Por favor, complete todos los campos"), $(n).removeClass("error").removeClass("success"), $.ajax({
                type: "POST",
                url: $(e).attr("action"),
                data: t
            }).done(function(e) {
                $(n).removeClass("error").delay(3e3).slideUp(), $(n).addClass("success").delay(3e3).slideUp(), $(n).text(e), $("#name").val(""), $("#email").val(""), $("#message").val("")
            }).fail(function(e) {
                $(n).removeClass("success").delay(2e3).slideUp(), $(n).addClass("error").delay(2e3).slideUp(), "" !== e.responseText ? $(n).text(e.responseText) : $(n).text("Hubo un problema con su mensaje y no pudo ser enviado, por favor intente de nuevo.")
            })
        })
    }
}();