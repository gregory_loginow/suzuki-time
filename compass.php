<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-smart-interna.php' ?>

<section id="compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="product-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive phone-logo" src="img/compass/compass-logo.png">
            <p class="phone-modelo">sp-54g</p>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive compass-phone-interna" src="img/compass/phone-02.png">  
  <div class="container-fluid iconos-producto-cintillo">
    <div class="row">              
      <div class="container">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">8 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="pantalla-compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-size-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2 kill-padding">
            <p class="interna-cintillo-right-title">pantalla<br><span>5”</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-8">
            <p class="interna-cintillo-right-text"><span>5.0 pulgadas pantalla de gran tamaño.<br>Todo el mundo está en su opinión</span><br>Cuenta con una pantalla de 5,0 pulgadas con diseño ultra estrecho de 2,5 mm, el área efectiva de pantalla alcanza 61.97 x 110.16 mm, con un brillo de hasta 500cd/m2. Gran visión, da vuelta tus sentidos!</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/compass/phone-03.png">             
      </div>
    </div>
  </div>
</section>

<section id="hd-compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/signal-icon.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-cyan">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">red<br><span>4g</span></p>         
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text-white"><span>HACE TU VIDA MAS FACIL</span><br>No usas redes 4G todavía? No estas a la ultima moda… COMPASS soporta redes LTE de alta velocidad de hasta 150 Mbps, lo que te permite decir adiós a los mensajes de “Cargando..."</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/compass/phone-04.png">             
      </div>
    </div>
  </div>
</section>

<section id="camara-compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/ram-icon.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-blue">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">memoria<br><span>1Gb Ram</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text-white">Sólo con el procesador Quad Core no es suficiente! 1 GB de RAM + 16 GB de ROM, esta gran capacidad de memoria, te ayuda a manejar fácilmente múltiples tareas. Sera como tu disco duro portátil, puede llevar todos tus archivos donde quieras!</p>
          </div>
        </div>
      </div>
      <div align="left" class="col-md-12 kill-padding image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/compass/phone-05.png">             
      </div>
    </div>
  </div>
</section>

<section id="cell-compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cell-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-small">procesador<br><span>quad core</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text">Sólo con el procesador Quad Core no es suficiente! 1 GB de RAM + 16 GB de ROM, esta gran capacidad de memoria, te ayuda a manejar fácilmente múltiples tareas. Sera como tu disco duro portátil, puede llevar todos tus archivos donde quieras!</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/compass/phone-06.png">             
      </div>
    </div>
  </div>
</section>

<section id="sim-compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/android-icon.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Android<br>Lolipop<br><span>5.1</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-4">
            <p class="interna-cintillo-right-text">Divertirte descubriendo y disfrutando las nuevas ventajas de este nuevo sistema operativo de Google. No podrás volver atrás, una vez hayas vivido esta experiencia.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">         
      </div>
    </div>
  </div>
</section>

<section id="bateria-compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/bat-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-black">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">bateria<br><span>2300<span class="mah">mAh</span></span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-4">
            <p class="interna-cintillo-right-text-white">Excelente capacidad, con 2800 mAh de carga que te proporcionaran muchas horas de satisfacción. Para el entretenimiento y el trabajo.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/compass/phone-07.png">             
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <div class="col-md-4">
            <p class="info-section-title">Caracteristicas Basicas</p>
            <p class="info-section-text">
              <span>Sitema Operativo:</span> Android 5.1 (lolipop)<br>
              <span>Dimensiones:</span> 156*76.8*7.5mm （L×W×H）<br>
              <span>Peso:</span> 160g<br>
              <span>CPU:</span> Quad core 1.3Ghz Processor (MT6582 Cortex A7)
            </p>
            <p class="info-section-title">Cámara</p>
            <p class="info-section-text">
              <span>Camara Trasera:</span> 13 Mp<br>
              <span>Camara Frontal:</span> 5 Mp<br>
              <span>Flash:</span> Si
            </p>
            <p class="info-section-title">Pantalla</p>
            <p class="info-section-text">
              <span>Tamaño de Pantalla:</span> 5”<br>
              <span>Calidad de Pantalla:</span> FWVGA IPS LCD<br>
              <span>Resolusion:</span> 1280 x 720
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Red</p>
            <p class="info-section-text">
              <span>SIM:</span> WCDMA/GSM Dual SIM Dual Standby<br>
              <span>Frecuencias:</span> WCDMA 850/1900/2100 & GSM 850/900/1800/1900
            </p>  
            <p class="info-section-title">Interfaz</p>
            <p class="info-section-text">
              <span>GPS:</span>Soporta <br>
              <span>Bluetooth:</span> Soporta<br>
              <span>WIFI:</span> Soporta 
            </p> 
            <p class="info-section-title">Bateria</p>
            <p class="info-section-text">
              <span>Capacidad:</span> 2800mAh polymer<br>
            </p> 
            <p class="info-section-title">Sensores</p>
            <p class="info-section-text">
              <span>Sensor de Movimiento:</span> Soporta<br>
              <span>G-Sensor:</span> Soporta
            </p>   
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Multimedia</p>
            <p class="info-section-text">
              <span>Musica:</span> MP3, wav, wma, ogg, ape, flac, aac, etc.<br>
              <span>Video:</span> H.264/263, MPEG4, VP8，MVC, etc.<br>
              <span>Imágenes:</span> JPEG, PNG, GIF, BMP, etc.
            </p> 
            <p class="info-section-title">Memoria</p>
            <p class="info-section-text">
              <span>Memoria Interna:</span> 8GB<br>
              <span>Memoria RAM:</span> 1GB<br>
              <span>Memoria Externa:</span> Micro SD card ( T-FLASH card), max 32GB
            </p>  
            <p class="info-section-title">Camara de Video</p>
            <p class="info-section-text">
              <span>Grabacion de Video:</span> 720P HD 60Frame/s<br>
            </p>  
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>