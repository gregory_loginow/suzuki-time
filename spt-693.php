<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-car-interna.php' ?> 

<section id="spt-693" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">CAR AUDIO BLUETOOTH</h2>
            <p class="equipo-modelo">spt-693</p>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive spt693-audio-interna" src="img/spt693/car-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/call-icon.png">
              </th>
              <th>
                <img src="img/assets/potencia-02-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Telefono<br>Manos libres</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Potencia</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Card</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="caracteristicas">
  <div class="container-fuid container-video-pad">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-2 kill-padding">
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Impedancia: 4 Ohm</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Respuesta de frecuencia: 40Hz-22kHz </p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Sonido envolvente con goma butílica para respuesta más suave.</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Altavoces de 3 vías</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Potencia: 250W</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Material del cono: Polipropileno</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Speaker traseros</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Tamaño 6x9"</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Tweeter Mylar 1.2"</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>