<?php include 'assets/header.php' ?>

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a class="active-navbar" href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="contact" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">Contactenos</h2>
            <p class="equipo-modelo">Contact Us</p>              
          </div>        
        </div>
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid contactos-pad">
    <div class="row botonera-footer">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <ul class="botonera-carousel">
            <li>                
              <a class="btn-contactos" href="#contacto">Contacto</a>
            </li>
            <li>
              <a class="btn-contactos" href="#soporte">Soporte</a>
            </li>
          </ul>
        </div>
      </div>          
    </div>
    <div class="container">
      <div class="col-md-10 col-md-offset-2">      
        <div class="col-md-4">
          <div class="owl-carousel">
            <div class="item" data-hash="contacto">
              <h3 class="contactos-title">CONTACTO</h3>
              <p class="contactos-text"><span>Oficinas Comerciales</span></p>
              <p class="contactos-text">Guemes 695<br>
              Vte. López (1638)<br>
              Buenos Aires, Argentina</p>
              <hr>
              <p class="datos-title"><img class="img-responsive tel-icon" src="img/assets/tel-icon.png"> TEL/FAX:</p>
              <p class="datos-text">(54 11) 4796-1006 y Rot</p>
              <p class="datos-title"><img class="img-responsive mail-icon" src="img/assets/mail-icon.png"> email</p>
              <p class="datos-text"><a href="mailto:info@suzukitime.com">info@suzukitime.com</a></p>
            </div>
            <div class="item" data-hash="soporte">
              <h3 class="contactos-title">SOPORTE TECNICO</h3>
              <p class="contactos-text">Te brindamos asesoramiento, instalación, configuración, reparación para todos tus dispositivos tecnológicos.</p>
              <hr>
              <p class="datos-title"><img class="img-responsive tool-icon" src="img/assets/tool-icon.png"> TEL/FAX:</p>
              <p class="contactos-text"><span>0800-444-1212</span><br>
              Llamanos y nuestros expertos de brindaran toda la informacion necesaria
              </p>
              <p class="datos-title"><img class="img-responsive mail-icon" src="img/assets/mail-icon.png"> email</p>
              <p class="contactos-text"><a href="mailto:info@suzukitime.com">soporte@suzukitime.com</a><br>
              Te brindamos soporte OnLine para que puedas resolver todo desde la comodidad de tu casa.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <p class="form-title">Consulta Rápida</p>
          <form id="contact-form" method="post" action="mailer/mailer.php">
            <input type="text" id="name" name="name" placeholder="Nombre" required>
            <input type="email" id="email" name="email" placeholder="Email" required>
            <textarea id="message" name="message" rows="6" placeholder="Mensaje" required></textarea>
            <button class="btn-form-send btn btn-primary standard-button2 ladda-button" type="submit"><i class="fa fa-caret-right"></i> Enviar</button>
            <div id="form-messages"></div>
          </form>
          <!--form id="contact-form" method="POST" action="mailer/mailer.php" class="contact-form form-pad">
            <input class="" type="text" id="name" name="name" placeholder="Nombre" required>
            <input class="" type="email" id="email" name="email" placeholder="E-mail" required>
            <textarea class="" id="message" name="message" rows="6" placeholder="Mensaje" required></textarea>
            <button class="btn-form-send btn btn-primary standard-button2 ladda-button" type="submit" id="submit" name="submit" data-style="expand-left"><i class="fa fa-caret-right"></i> Enviar</button>
            <div id="form-messages"></div-->
          </form>
        </div>
      </div>         
    </div>       
  </div>
</section>

<?php include 'assets/footer.php' ?>