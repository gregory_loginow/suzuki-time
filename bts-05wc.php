<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-audio-interna.php' ?>   

<section id="bts-05wc" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="info-audio-cintillo-interna">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">SPEAKER BLUETOOTH</h2>
            <p class="equipo-modelo">bts-05wc</p>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive bts05wc-audio-interna" src="img/bts05wc/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-10 col-md-offset-1">              
          <div class="col-md-4">
            <p class="info-section-title">Tu música en cualquier lugar</p>
            <p class="info-section-text">
              Con batería interna de larga duración, este equipo portátil es ideal para tus salidas al aire libre o simplemente para disfrutar de la tranquilidad de tu hogar. 
            </p>
            <p class="info-section-title">Reproducí toda la música en formato MP3</p>
            <p class="info-section-text">
              Gracias al exclusivo puerto USB, podras acceder a música en formato MP3 y al mismo momento cargar tu dispositivo móvil y controlar las pistas desde el equipo o el control remoto. 
            </p>
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Radio/Reloj y alarma</p>
            <p class="info-section-text">
              Descansa tranquilo y despertate con el sonido de tu emisora de radio o canción preferida.
            </p>  
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Diversión inalámbrica</p>
            <p class="info-section-text">
              Enlaza tu dispositivo móvil como tablet o Smartphone via Bluetooth y tendrás tu música en cualquier lugar con el mejor sonido. 
            </p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>