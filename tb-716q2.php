<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-tablet-interna.php' ?>

<section id="tb-716q2" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="info-tablet-cintillo-interna">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TABLET TOUCH</h2>
            <p class="equipo-modelo">tb-716q2</p>             
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive tb716qb-tablet-interna" src="img/tb716qb/tablet-01.png">  
  <div class="container-fluid info-tablet-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-8 col-md-offset-2 kill-padding">
          <div class="col-md-2 col-md-offset-1">
            <img src="img/assets/bluetooth-icon.png">
            <p class="texto-info-cintillo">Bluetooth</p>
          </div>
          <div class="col-md-2">
            <img src="img/assets/cell-icon.png">
            <p class="texto-info-cintillo">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img src="img/assets/tablet-size-icon.png">
            <p class="texto-info-cintillo">Pantalla 7" HD</p>
          </div>
          <div class="col-md-2">
            <img src="img/assets/16gb-icon.png">
            <p class="texto-info-cintillo">Memoria Flash</p>
          </div>
          <div class="col-md-2">
            <img src="img/assets/cam-icon.png">
            <p class="texto-info-cintillo">Dual Camara</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="caracteristicas">
  <div class="container-fuid">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-1 kill-padding">
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Bluetooth</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> 7” <span>(17.5cm) Capacitiva/Multi-Touch</span></p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Dual Camera</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> High Definition</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Memoria interna 1 GB DDR3</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Memoria Flash 16GB</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Rockchip 3128 QUAD CORE , Cortex A7</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Android 4.4</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-purple"></i> Wifi <span>802.11 B/G/N</span> y 3G <span>con conexion USB (Wcdma/GSM)</span></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-10 col-md-offset-1">              
          <div class="col-md-4">
            <p class="info-section-title">Conexión Bluetooth</p>
            <p class="info-section-text">
              Aprovecha todo el potencial de tu equipo, conectándolo a miles de periféricos que tea harán hacer todo mas fácil: teclado, mouse, auriculares. Todo eso y mucho mas, te harán sentir que tienes la mejor computadora.
            </p>
            <p class="info-section-title">Memoria incorporada, y también externa</p>
            <p class="info-section-text">
            Tendrás 8GB de capacidad interna para disponer de ella como quieras. Aplicaciones, o todo tu contenido multimedia. Pero podrás adicionarle mayor capacidad a partir del slot de tarjeta MicroSD, que soporta hasta 32GB extra!
            </p>
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Pantalla HD</p>
            <p class="info-section-text">
              La mejor pantalla para las mejores imágenes! Vas a disfrutar de todos tus videos y fotos favoritas con la mejor calidad de imagen. También, te dará la mejor sensibilidad en su pantalla a partir del sensor capacitivo multi-touch.
            </p>  
            <p class="info-section-title">Doble Cámara, el doble de imágenes</p>
            <p class="info-section-text">
              Con una cámara principal que podrás usar para saca las mejores fotos, perfectas y claras. Con la frontal podrás divertirte sacando selfies, o hacer video llamadas a tus afectos mas querido. 
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Quad Core</p>
            <p class="info-section-text">
              Cuatro núcleos a la mayor potencia, para que nada vaya lento. Te dará todo lo que le pidas. La mejor ayuda, para la los mas exigentes.
            </p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>