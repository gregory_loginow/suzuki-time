<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-car-interna.php' ?> 

<section id="cbs-511" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">CAR AUDIO BLUETOOTH</h2>
            <p class="equipo-modelo">cbs-511</p>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive cbs511-audio" src="img/cbs511/car-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/call-icon.png">
              </th>
              <th>
                <img src="img/assets/potencia-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Telefono<br>Manos libres</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Potencia</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Card</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="caracteristicas">
  <div class="container-fuid container-video-pad">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-1 kill-padding">
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-blue"></i>  Función Bluetooth</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-blue"></i>  Sincronizacion de teléfono celular para conversación con manos libres</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-blue"></i>  Ranura de tarjeta de memoria SD</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-blue"></i>  Puerto USB apto para reproducción de dispositivos portables</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-blue"></i>  Reproducción de sonido Mp3</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-blue"></i>  Potencia de salida  45W x 4 canales</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>