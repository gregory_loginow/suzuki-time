<?php include 'assets/header.php' ?>

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a class="active-navbar" href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="empresa" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">Sobre  Nosotros</h2>
            <p class="equipo-modelo">About Us</p>              
          </div>        
        </div>
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-8 col-md-offset-2 kill-padding">   
          <p class="info-empresa-section-title">Sobre Suzuki Time Electronics</p>
          <p class="info-empresa-section-text">
            Nuestra empresa lleva 20 años en Argentina y cuenta con una amplia experiencia en tecnología de alta calidad, ofreciendo productos electrónicos con cobertura integral de comunicación.
          </p>
          <p class="info-empresa-section-text">
            Acompañamos nuestra marca y cada uno de sus productos con estrategias de marketing enfocadas en acciones puntuales y campañas publicitarias, a lo largo de todo el país.
          </p>
          <p class="info-empresa-section-text">
            Nuestros servicios crecieron exponencialmente desde 1993 y logramos establecer una gran capacidad de respuesta a través de los departamentos de asesoramiento y atención de consultas.
          </p>
          <p class="info-empresa-section-text">
            Actualmente ofrecemos una amplia red consolidada de servicios técnicos oficiales, distribuidos esos 140 services en ciudades y capitales del interior; así como también el soporte de un service central ubicado en un punto estratégico del conurbano bonaerense.
          </p>
          <p class="info-empresa-section-text">
            Hoy nos enfocamos en ofrecerle lo que marca la tendencia en el mundo tecnológico.
          </p>
          <p class="info-empresa-section-text">
            Con nuestra gama de productos de alta prestación SZK, brindamos niveles top en estética y presentación de diseño. Tal es el caso de los micro proyectores SZK o los micro systems SZK, desarrollados con tecnología de avanzada y considerado como lo ultimo en novedades de esta línea .En el mismo nivel , están los reproductores de video SZK portátiles , los SZK home theatre, los SZK dvd y los SZK radi grabadores. todos productos de vanguardia.
          </p>
          <p class="info-empresa-section-text">
            Nuestro compromiso de servicio continuo en la elaboración de una política de precios adecuada al mercado , que coloca a SUZUKI TIME en la posición privilegiada en el sector y nos permite ofrecer productos excelentes a precios accesibles.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>