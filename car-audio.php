<?php include 'assets/header.php' ?>    

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>            
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a href="video.php">Video</a></li>
            <li><a class="active-navbar" href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid header-subclass is-visible" data-nav-status="toggle">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-2 kill-padding">
          <h6>CAR AUDIO</h6>
          <ul class="subclass-list">
            <li><a class="subclass-active" href="#cbs-511">CBS-511</a></li>
            <li><a href="#cbs-512">CBS-512</a></li>
            <li><a href="#spt-693">SPT-693</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="cbs-511" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">CAR AUDIO BLUETOOTH</h2>
            <p class="equipo-modelo">cbs-511</p><a class="ver-mas-btn" href="cbs-511.php"><i class="fa fa-caret-right"></i> Ver más</a>       
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive cbs511-audio" src="img/cbs511/car-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/call-icon.png">
              </th>
              <th>
                <img src="img/assets/potencia-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Telefono<br>Manos libres</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Potencia</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Card</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="cbs-512" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">CAR AUDIO BLUETOOTH</h2>
            <p class="equipo-modelo">cbs-512</p><a class="ver-mas-btn" href="cbs-512.php"><i class="fa fa-caret-right"></i> Ver más</a>       
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive cbs512-audio-home" src="img/cbs512/car-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/potencia-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">CD</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Potencia</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Card</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="spt-693" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">CAR AUDIO BLUETOOTH</h2>
            <p class="equipo-modelo">spt-693</p><a class="ver-mas-btn" href="spt-693.php"><i class="fa fa-caret-right"></i> Ver más</a>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive spt693-audio-home" src="img/spt693/car-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/call-icon.png">
              </th>
              <th>
                <img src="img/assets/potencia-02-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Telefono<br>Manos libres</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Potencia</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Card</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>