<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-smart-interna.php' ?>

<section id="level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="product-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive phone-logo" src="img/level/level-logo.png">
            <p class="phone-modelo">sp-56</p>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive level-phone-interna" src="img/level/phone-02.png">  
  <div class="container-fluid iconos-producto-cintillo">
    <div class="row">              
      <div class="container">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">13 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería 2000 mAh</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="pantalla-level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-size-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">pantalla<br><span>5"</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text"><span>Realmente Perfecto</span><br>Las mejores tecnologías equipan este teléfono. Con un diseño perfecto, 5 pulgadas es el tamaño perfecto para manejarlo con una sola mano. Realmente muy agradable.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/level/phone-03.png">             
      </div>
    </div>
  </div>
</section>

<section id="hd-level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-hd-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-cyan">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">resolución<br><span class="interna-cintillo-right-text-span">1280 x 720</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text-white"><span>Hace realidad tu Sueño</span><br>5.0 pulgadas, 1280 x 720 dpi y 267 ppp. Fotos, películas y juegos, color con un color vivido, sentirte mas cómodo.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-10 image-pad">
        <img class="img-responsive image-pad" src="img/level/phone-04.png">             
      </div>
    </div>
  </div>
</section>

<section id="cell-level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cell-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">procesador<br><span>quad core</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text"><span>UNA EXPERIENCIA INCREIBLE</span><br>CPU MTK6582 de 1,3 GHz, Cortex A7 que brinda un aumento del 30% en la velocidad de procesamiento. Sistema Operativo Android 4.4, su fluidez es increíble. LEVEL está listo para tu mejor experiencia.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/level/phone-05.png">             
      </div>
    </div>
  </div>
</section>

<section id="sim-level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title"><span>Marco</span><br><span class="interna-cintillo-right-text-span">metalico<br>de</span><span> 5mm</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-4">
            <p class="interna-cintillo-right-text">Extraordinariamente delgado. Una experiencia sorprendente.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/level/phone-06.png">             
      </div>
    </div>
  </div>
</section>

<section id="bateria-level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/bat-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-black">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">bateria<br><span>2000<span class="mah">mAh</span></span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text-white">Batería de ion-litio de 1600 mAh y un sistema gestión inteligente de la energía te proporcionan un día entero de uso con una sola carga. Es decir, tiene una capacidad mayor que lo normal y mejora la vida de la batería en un 10%. Axis garantiza que la batería siempre tiene energía, no importa lo ocupado que estés.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/level/phone-07.png">             
      </div>
    </div>
  </div>
</section>

<section id="camara-level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cam-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">cámara<br><span class="mah">TRASERA</span><span class="interna-cintillo-right-title-small"> 13 Mpx</span><br><span class="mah">FRONTAL</span><span class="interna-cintillo-right-title-small"> 5 Mpx</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-4">
            <p class="interna-cintillo-right-text">Con la cámara principal de 8MP saca las mejores fotos, perfectas y claras. Capturar y compartir tus momentos favoritos nunca ha sido más fácil y divertido.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/level/phone-08.png">             
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <div class="col-md-4">
            <p class="info-section-title">Caracteristicas Basicas</p>
            <p class="info-section-text">
              <span>Sitema Operativo:</span> Android 4.4 (KitKat)<br>
              <span>Dimensiones:</span> 145*71.2*6.9mm （L×W×H）<br>
              <span>Peso:</span> 135g<br>
              <span>CPU:</span> Quad core 1.3Ghz Processor (MT6582 Cortex A7)
            </p>
            <p class="info-section-title">Cámara</p>
            <p class="info-section-text">
              <span>Camara Trasera:</span> 13 Mp<br>
              <span>Camara Frontal:</span> 5 Mp<br>
              <span>Flash:</span> Si
            </p>
            <p class="info-section-title">Pantalla</p>
            <p class="info-section-text">
              <span>Tamaño de Pantalla:</span> 5”<br>
              <span>Calidad de Pantalla:</span> FWVGA IPS LCD<br>
              <span>Resolusion:</span> 1280 x 720
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Red</p>
            <p class="info-section-text">
              <span>SIM:</span> WCDMA/GSM Dual SIM Dual Standby<br>
              <span>Frecuencias:</span> WCDMA 850/1900/2100 & GSM 850/900/1800/1900
            </p>  
            <p class="info-section-title">Interfaz</p>
            <p class="info-section-text">
              <span>GPS:</span>Soporta <br>
              <span>Bluetooth:</span> Soporta<br>
              <span>WIFI:</span> Soporta 
            </p> 
            <p class="info-section-title">Bateria</p>
            <p class="info-section-text">
              <span>Capacidad:</span> 2000mAh polymer<br>
            </p> 
            <p class="info-section-title">Sensores</p>
            <p class="info-section-text">
              <span>Sensor de Movimiento:</span> Soporta<br>
              <span>G-Sensor:</span> Soporta
            </p>   
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Multimedia</p>
            <p class="info-section-text">
              <span>Musica:</span> MP3, wav, wma, ogg, ape, flac, aac, etc.<br>
              <span>Video:</span> H.264/263, MPEG4, VP8，MVC, etc.<br>
              <span>Imágenes:</span> JPEG, PNG, GIF, BMP, etc.
            </p> 
            <p class="info-section-title">Memoria</p>
            <p class="info-section-text">
              <span>Memoria Interna:</span> 8GB<br>
              <span>Memoria RAM:</span> 1GB<br>
              <span>Memoria Externa:</span> Micro SD card ( T-FLASH card), max 32GB
            </p>  
            <p class="info-section-title">Camara de Video</p>
            <p class="info-section-text">
              <span>Grabacion de Video:</span> 720P HD 60Frame/s<br>
            </p>  
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>