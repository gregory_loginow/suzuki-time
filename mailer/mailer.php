<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = strip_tags(trim($_POST["name"]));
                $name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $message = trim($_POST["message"]);

        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            echo "Hubo un problema con su mensaje y no pudo ser enviado, por favor intente de nuevo.";
            exit;
        }

        
        //$recipient = "gregory@fpestudio.com";
        $recipient = "info@suzukitime.com";

        $subject = "New contact from ".$name;

        $email_content = "Nombre: $name\n\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "Mensaje: $message\n\n";

        $email_headers = "From: $name <$email>";

        if (mail($recipient, $subject, $email_content, $email_headers)) {
            echo "Gracias! Tu mensaje ha sido enviado.";
        } else {
            echo "Disculpe, algo ha sucedido y su mensaje no pudo ser enviado.";
        }

    } else {
        echo "Hubo un problema con su mensaje y no pudo ser enviado, por favor intente de nuevo.";
    }

?>