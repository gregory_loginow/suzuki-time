<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-audio-interna.php' ?>   

<section id="sp-130" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="info-audio-cintillo-interna">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TORRE PARLANTE</h2>
            <p class="equipo-modelo">sp-130</p>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive sp130-audio-internas" src="img/sp130/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/guitar-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Guitarra</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB<br>•Rec</p>
              </th>
              <th>
                <p class="texto-info-cintillo">MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-10 col-md-offset-1">              
          <div class="col-md-4">
            <p class="info-section-title">Tu DJ portátil</p>
            <p class="info-section-text">
              Con un exclusivo sistema de manija portante, ruedas y batería de larga duración, este sistema de audio portátil es el mejor aliado para tus fiestas.
            </p>
            <p class="info-section-title">Ecualizador para obtener el mejor sonido</p>
            <p class="info-section-text">
              Con el equalizador grafico de 5 canales, vas a poder encontrar la mejor fidelidad y potencia para tu música. 
            </p>
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Karaoke y guitarra</p>
            <p class="info-section-text">
              Conecta tu guitarra y micrófono para hacer Karaoke. Disfruta de la música y hace tus propias versiones de tus temas preferidos.
            </p>  
            <p class="info-section-title">Potencia Inalámbrica</p>
            <p class="info-section-text">
              Conecta tu dispositivo portátil como Tablet o Smartphone a través de Bluetooth y disfruta de tu música de manera increíble. Con 4200 W de potencia, vas a tener potente sonido con la mejor fidelidad. 
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Graba tu música en formato MP3</p>
            <p class="info-section-text">
              Podras grabar tus creaciones a travez del puerto USB.  
            </p> 
            <p class="info-section-title">Radio FM y Tarjeta SD</p>
            <p class="info-section-text">
              Sintoniza tu emisora de FM preferida o reproducí música en formato MP3 desde una tarjeta SD. 
            </p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>