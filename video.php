<?php include 'assets/header.php' ?>

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>            
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a class="active-navbar" href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid header-subclass is-visible" data-nav-status="toggle">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-2 kill-padding">
          <h6>VIDEO</h6>
          <ul class="subclass-list">
            <li><a class="subclass-active" href="#dvp-720g">DVP-720G</a></li>
            <li><a href="#dvp-920g">DVP-920G</a></li>
            <li><a href="#vr-101b">VR-101B</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="dvp-720g" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">DVD PORTATIL</h2>
            <p class="equipo-modelo">dvp-720g</p><a class="ver-mas-btn" href="dvp-720g.php"><i class="fa fa-caret-right"></i> Ver más</a>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive dvp720g-audio-home" src="img/dvp720g/video-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-8 col-md-offset-2">
          <div class="col-md-2 col-md-offset-1">
            <img class="img-responsive" src="img/assets/car-icon.png">
            <p class="iconos-producto-texto">Juegos </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/joystick-icon.png">
            <p class="iconos-producto-texto">Juegos </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/usb-icon.png">
            <p class="iconos-producto-texto">USB</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/mp3-icon.png">
            <p class="iconos-producto-texto">Card</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/speaker-icon.png">
            <p class="iconos-producto-texto">Sonido Estereo</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="dvp-920g" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">DVD PORTATIL</h2>
            <p class="equipo-modelo">dvp-920g</p><a class="ver-mas-btn" href="dvp-920g.php"><i class="fa fa-caret-right"></i> Ver más</a>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive dvp920g-audio-home" src="img/dvp920g/video-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-8 col-md-offset-2">
          <div class="col-md-2 col-md-offset-1">
            <img class="img-responsive" src="img/assets/car-icon.png">
            <p class="iconos-producto-texto">Juegos </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/joystick-icon.png">
            <p class="iconos-producto-texto">Juegos </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/usb-icon.png">
            <p class="iconos-producto-texto">USB</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/mp3-icon.png">
            <p class="iconos-producto-texto">Card</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/speaker-icon.png">
            <p class="iconos-producto-texto">Sonido Estereo</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="vr-101b" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">VIRTUAL REALITY GLASSES</h2>
            <p class="equipo-modelo">vr-101b</p><a class="ver-mas-btn"><i class="fa fa-caret-right"></i> Ver más</a>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive vr-101b-home" src="img/vr-101b/vr-01.png">  
  <div class="container-fluid info-audio-cintillo">    
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/3d-icon.png">
            <p class="iconos-producto-texto">Visualizador de videos 3D</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/diameter-icon.png">
            <p class="iconos-producto-texto">Diámetro de lente 42mm.</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/lens-icon.png">
            <p class="iconos-producto-texto">Lente óptico</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-size-icon.png">
            <p class="iconos-producto-texto">Smartphones desde 4,7" hasta 5,5"</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/focus-icon.png">
            <p class="iconos-producto-texto">Ajuste de foco</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/weight-icon.png">
            <p class="iconos-producto-texto">Materiales soft</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>    
