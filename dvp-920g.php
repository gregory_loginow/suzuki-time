<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-video-interna.php' ?> 

<section id="dvp-920g" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">DVD PORTATIL</h2>
            <p class="equipo-modelo">dvp-920g</p>
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive dvp920g-audio-interna" src="img/dvp920g/video-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-8 col-md-offset-2">
          <div class="col-md-2 col-md-offset-1">
            <img class="img-responsive" src="img/assets/car-icon.png">
            <p class="iconos-producto-texto">Juegos </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/joystick-icon.png">
            <p class="iconos-producto-texto">Juegos </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/usb-icon.png">
            <p class="iconos-producto-texto">USB</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/mp3-icon.png">
            <p class="iconos-producto-texto">Card</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/speaker-icon.png">
            <p class="iconos-producto-texto">Sonido Estereo</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="caracteristicas">
  <div class="container-fuid container-video-pad">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-1 kill-padding">
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Pantalla giratoria (sistema Swivel) de 7”</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Puerto USB / SD CARD</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Sistema Anti-Shock</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Bolso / Soporte DVD para auto</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Control remoto full ultra slim</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Formato 16:9</p>
          </div>
          <div class="col-md-4">
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  CD 200 juegos + 1 Gamepad</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Cargador para auto</p>
            <p class="caracteristicas-tablets"><i class="fa fa-caret-right select-verde"></i>  Parlantes incorporados Stereo</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>