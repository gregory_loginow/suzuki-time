<?php include 'assets/header.php' ?>

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a class="active-navbar" href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid header-subclass is-visible" data-nav-status="toggle">
    <div class="row">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <h6>TABLETS</h6>
          <ul class="subclass-list">
            <li><a class="subclass-active" href="#tb-78b">TB-78B</a></li>
            <li><a href="#tb-78hd">TB-78HD</a></li>
            <li><a href="#tb-78q2">TB-78Q2</a></li>
            <li><a href="#tb-716q2">TB-716Q2</a></li>
          </ul>
        </div>
      </div>
    </div>        
  </div>
</header>

<section id="tb-78b" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TABLET TOUCH</h2>
            <p class="equipo-modelo">tb-78b</p><a class="ver-mas-btn" href="tb-78b.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive tb78b-tablet-home" src="img/tb78b/tablet-01.png">  
  <div class="container-fluid info-tablet-cintillo">
    <div class="row">              
      <div class="container cintillo-tablet-pad">
        <div align="center" class="col-md-8 col-md-offset-2 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/cell-02-icon.png">
              </th>
              <th>
                <img src="img/assets/tablet-size-icon.png">
              </th>
              <th>
                <img src="img/assets/8gb-icon.png">
              </th>
              <th>
                <img src="img/assets/cam-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Dual Core</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Pantalla 7" HD</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Memoria Flash</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Dual Camara</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="tb-78hd" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TABLET TOUCH</h2>
            <p class="phone-modelo">tb-78hd</p><a class="ver-mas-btn" href="tb-78hd.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive tb78hd-tablet-home" src="img/tb78hd/tablet-01.png">  
  <div class="container-fluid info-phone-cintillo">
    <div class="row">              
      <div class="container cintillo-tablet-pad">
        <div align="center" class="col-md-8 col-md-offset-2 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/cell-02-icon.png">
              </th>
              <th>
                <img src="img/assets/tablet-size-icon.png">
              </th>
              <th>
                <img src="img/assets/8gb-icon.png">
              </th>
              <th>
                <img src="img/assets/cam-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Dual Core</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Pantalla 7" HD</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Memoria Flash</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Dual Camara</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="tb-78q2" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TABLET TOUCH</h2>
            <p class="equipo-modelo">tb-78q2</p><a class="ver-mas-btn" href="tb-78q2.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive tb78qb-tablet-home" src="img/tb78qb/tablet-01.png">  
  <div class="container-fluid info-tablet-cintillo">
    <div class="row">              
      <div class="container cintillo-tablet-pad">
        <div align="center" class="col-md-8 col-md-offset-2 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/cell-icon.png">
              </th>
              <th>
                <img src="img/assets/tablet-size-icon.png">
              </th>
              <th>
                <img src="img/assets/8gb-icon.png">
              </th>
              <th>
                <img src="img/assets/cam-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Quad Core</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Pantalla 7" HD</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Memoria Flash</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Dual Camara</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="tb-716q2" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TABLET TOUCH</h2>
            <p class="equipo-modelo">tb-716q2</p><a class="ver-mas-btn" href="tb-716q2.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive tb716qb-tablet-home" src="img/tb716qb/tablet-01.png">  
  <div class="container-fluid info-tablet-cintillo">
    <div class="row">              
      <div class="container cintillo-tablet-pad">
        <div align="center" class="col-md-8 col-md-offset-2 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/cell-icon.png">
              </th>
              <th>
                <img src="img/assets/tablet-size-icon.png">
              </th>
              <th>
                <img src="img/assets/8gb-icon.png">
              </th>
              <th>
                <img src="img/assets/cam-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Quad Core</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Pantalla 7" HD</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Memoria Flash</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Dual Camara</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>