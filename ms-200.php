<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-audio-interna.php' ?>   

<section id="ms-200" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="info-audio-cintillo-interna">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MINISISTEMA</h2>
            <p class="equipo-modelo">ms-200</p>         
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive ms200-audio-interna" src="img/ms200/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">             
        <div align="center" class="col-md-10 col-md-offset-2">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bluetooth-icon.png">
            <p class="iconos-producto-texto">Bluetooth</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/remote-icon.png">
            <p class="iconos-producto-texto">Control Remoto</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/usb-icon.png">
            <p class="iconos-producto-texto">USB</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/disc-icon.png">
            <p class="iconos-producto-texto">CD/MP3</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/radio-icon.png">
            <p class="iconos-producto-texto">FM Digital</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-10 col-md-offset-1">              
          <div class="col-md-4">
            <p class="info-section-title">Potencia Inalámbrica</p>
            <p class="info-section-text">
              Conecta tu dispositivo portátil como Tablet o Smartphone a través de Bluetooth y disfruta de tu música de manera increíble. Con 6600 W de potencia, vas a tener potente sonido con la mejor fidelidad.
            </p>
            <p class="info-section-title">Entrada de audio auxiliar</p>
            <p class="info-section-text">
              Podras escuchar música o el audio de otros dispositivos como TV o DVD y asi tener un sistema integrado de audio en tu hogar.
            </p>
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Reproducí toda la música en formato MP3</p>
            <p class="info-section-text">
              Gracias al exclusivo puerto USB, podras acceder a música en formato MP3 y al mismo momento cargar tu dispositivo móvil y controlar las pistas desde el equipo o el control remoto.
            </p>  
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Sintonizador digital</p>
            <p class="info-section-text">
              Selecciona de forma digital tu FM favorita y guarda la emisora para que puedas volver a escucharla cuando quieras.
            </p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section-->

<section id="pantalla-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-size-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Bluetooth</p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text"><span>Potencia Inalámbrica</span><br>Conecta tu dispositivo portátil como Tablet o Smartphone a través de Bluetooth y disfruta de tu música de manera increíble. Con 6600 W de potencia, vas a tener potente sonido con la mejor fidelidad.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-03.png">             
      </div>
    </div>
  </div>
</section>

<section id="hd-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-hd-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Entrada de audio <br>auxiliar</p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text">Podras escuchar música o el audio de otros dispositivos como TV o DVD y asi tener un sistema integrado de audio en tu hogar.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-05.png">             
      </div>
    </div>
  </div>
</section>

<section id="cell-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cell-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title"><span>USB</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text"><span>Reproducí toda la música en formato MP3</span><br>Gracias al exclusivo puerto USB, podras acceder a música en formato MP3 y al mismo momento cargar tu dispositivo móvil y controlar las pistas desde el equipo o el control remoto.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-06.png">             
      </div>
    </div>
  </div>
</section>

<section id="sim-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/dual-sim-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Radio<br><span>FM</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text"><span>Sintonizador digital</span><br>Selecciona de forma digital tu FM favorita y guarda la emisora para que puedas volver a escucharla cuando quieras.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/axis/phone-07.png">             
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>