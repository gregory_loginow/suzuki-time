<?php include 'assets/header.php' ?>

<?php include 'assets/navbar-smart-interna.php' ?>

<section id="axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="product-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive phone-logo" src="img/axis/axis-logo.png">
            <p class="phone-modelo">sp-45</p>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive axis-phone-interna" src="img/axis/phone-02.png">  
  <div class="container-fluid iconos-producto-cintillo">
    <div class="row">              
      <div class="container">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">4,5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">8 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="pantalla-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-size-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">pantalla<br><span>4,5"</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text">La pantalla de 4,5” hace del Axis, que tenga un diseño súper compacto y excelente portabilidad sin perder cualquier detalle de calidad de todo lo que te gusta mirar en tu Smartphone.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-03.png">             
      </div>
    </div>
  </div>
</section>

<section id="camara-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cam-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-black">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">cámara<br><span>8 Mpx</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text-white">Con la cámara principal de 8MP saca las mejores fotos, perfectas y claras. Capturar y compartir tus momentos favoritos nunca ha sido más fácil y divertido.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/axis/phone-04.png">             
      </div>
    </div>
  </div>
</section>

<section id="hd-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-hd-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">pantalla<br><span>qhd ips</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text">Con una definición de pantalla de 960 x 540 pixeles y tecnología IPS de construcción, hace de este equipo tan exclusivo como único ya que estas características solo las podes ver en equipos de mayo costo.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-05.png">             
      </div>
    </div>
  </div>
</section>

<section id="cell-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cell-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">procesador<br><span>quad core</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text">Axis esta equipado con un procesador de cuatro núcleos que tiene muy bajo consumo de energía y alta velocidad. Es un equipo muy potente diseñado para ser increíblemente eficiente.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-06.png">             
      </div>
    </div>
  </div>
</section>

<section id="sim-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/dual-sim-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Dual<br><span>sim</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text">Con el doble zócalo de tarjeta SIM, podes disponer de hasta dos líneas telefónicas independientes. Es ideal para utilizar una para navegar y descargar los mejores contenidos, y con la otra para llamar a tus contactos favoritos.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/axis/phone-07.png">             
      </div>
    </div>
  </div>
</section>

<section id="bateria-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/bat-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-black">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">bateria<br><span>1600<span class="mah">mAh</span></span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text-white">Batería de ion-litio de 1600 mAh y un sistema gestión inteligente de la energía te proporcionan un día entero de uso con una sola carga. Es decir, tiene una capacidad mayor que lo normal y mejora la vida de la batería en un 10%. Axis garantiza que la batería siempre tiene energía, no importa lo ocupado que estés.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-08.png">             
      </div>
    </div>
  </div>
</section>

<section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <div class="col-md-4">
            <p class="info-section-title">Caracteristicas Basicas</p>
            <p class="info-section-text">
              <span>Sitema Operativo:</span> Android 4.4 (KitKat)<br>
              <span>Dimensiones:</span> 156*76.8*6.9mm（L×W×H）<br>
              <span>Peso:</span> 120g<br>
              <span>CPU:</span> Quad core  1.3Ghz (MT6582 Cortex A7)
            </p>
            <p class="info-section-title">Cámara</p>
            <p class="info-section-text">
              <span>Camara Trasera:</span> 8 Mp<br>
              <span>Camara Frontal:</span> 2 Mp<br>
              <span>Flash:</span> Si
            </p>
            <p class="info-section-title">Pantalla</p>
            <p class="info-section-text">
              <span>Tamaño de Pantalla:</span> 4.5”<br>
              <span>Calidad de Pantalla:</span> QHD IPS<br>
              <span>Resolusion:</span> 960 x 540
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Red</p>
            <p class="info-section-text">
              <span>SIM:</span> WCDMA/GSM Dual SIM Dual Standby<br>
              <span>Frecuencias:</span> WCDMA 850/1900/2100 & GSM 850/900/1800/1900
            </p>  
            <p class="info-section-title">Interfaz</p>
            <p class="info-section-text">
              <span>GPS:</span>Soporta <br>
              <span>Bluetooth:</span> Soporta<br>
              <span>WIFI:</span> Soporta 
            </p> 
            <p class="info-section-title">Bateria</p>
            <p class="info-section-text">
              <span>Capacidad:</span> 1600mAh<br>
            </p> 
            <p class="info-section-title">Sensores</p>
            <p class="info-section-text">
              <span>Sensor de Movimiento:</span> Soporta<br>
              <span>G-Sensor:</span> Soporta
            </p>   
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Multimedia</p>
            <p class="info-section-text">
              <span>Musica:</span> MP3, wav, wma, ogg, ape, flac, aac, etc.<br>
              <span>Video:</span> H.264/263, MPEG4, VP8，MVC, etc.<br>
              <span>Imágenes:</span> JPEG, PNG, GIF, BMP, etc.
            </p> 
            <p class="info-section-title">Memoria</p>
            <p class="info-section-text">
              <span>Memoria Interna:</span> 8GB<br>
              <span>Memoria RAM:</span> 1GB<br>
              <span>Memoria Externa:</span> Micro SD card ( T-FLASH card), max 32GB
            </p>  
            <p class="info-section-title">Camara de Video</p>
            <p class="info-section-text">
              <span>Grabacion de Video:</span> 720P HD 60Frame/s<br>
            </p>  
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>