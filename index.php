<?php include 'assets/header.php' ?>

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a class="active-navbar" href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a href="audio.php">Audio</a></li>
            <li><a href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid header-subclass is-visible" data-nav-status="toggle">
    <div class="row">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 kill-padding">
          <h6>SMARTPHONES</h6>
          <ul class="subclass-list">
            <li><a class="subclass-active" href="#axis">AXIS</a></li>
            <li><a href="#spin">SPIN</a></li>
            <li><a href="#level">LEVEL</a></li>
            <li><a href="#horizon">HORIZON</a></li>
            <li><a href="#compass">COMPASS</a></li>
          </ul>
        </div>
      </div>
    </div>        
  </div>
</header>

<section id="axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive producto-logo" src="img/axis/axis-logo.png">
            <p class="phone-modelo">sp-45</p><a class="ver-mas-btn" href="axis.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive axis-phone-home" src="img/axis/phone-01.png">  
  <div class="container-fluid info-phone-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">4,5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">8 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="spin" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive producto-logo" src="img/spin/spin-logo.png">
            <p class="phone-modelo">sp-50</p><a class="ver-mas-btn" href="spin.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive spin-phone-home" src="img/spin/phone-01.png">  
  <div class="container-fluid info-phone-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">8 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla FWVGA IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería 2000 mAh</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="level" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive producto-logo" src="img/level/level-logo.png">
            <p class="phone-modelo">sp-56</p><a class="ver-mas-btn" href="level.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive level-phone-home" src="img/level/phone-01.png">  
  <div class="container-fluid info-phone-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">13 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería 2000 mAh</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="horizon" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive producto-logo" src="img/horizon/horizon-logo.png">
            <p class="phone-modelo">sp-556</p><a class="ver-mas-btn" href="horizon.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive horizon-phone-home" src="img/horizon/phone-01.png">  
  <div class="container-fluid info-phone-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5,5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">13 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="compass" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <img class="img-responsive producto-logo" src="img/compass/compass-logo.png">
            <p class="phone-modelo">sp-54g</p><a class="ver-mas-btn" href="compass.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive compass-phone-home" src="img/compass/phone-01.png">  
  <div class="container-fluid info-phone-cintillo">
    <div class="row">              
      <div class="container cintillo-home-pad">
        <div align="center" class="col-md-10 col-md-offset-1">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-size-icon.png">
            <p class="iconos-producto-texto">5” Pantalla </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cam-icon.png">
            <p class="iconos-producto-texto">8 MP Cámara </p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/phone-hd-icon.png">
            <p class="iconos-producto-texto">Pantalla QHD IPS</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/cell-icon.png">
            <p class="iconos-producto-texto">Quad Core</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/dual-sim-icon.png">
            <p class="iconos-producto-texto">Dual SIM</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bat-icon.png">
            <p class="iconos-producto-texto">Batería</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>