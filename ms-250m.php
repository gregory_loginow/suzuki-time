<?php include 'assets/header.php' ?>   

<?php include 'assets/navbar-audio-interna.php' ?>   

<section id="ms-250m" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="info-audio-cintillo-interna">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MINISISTEMA</h2>
            <p class="equipo-modelo">ms-250m</p>   
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive ms250m-audio-home" src="img/ms250m/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">      
        <div align="center" class="col-md-10 col-md-offset-2">
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/bluetooth-icon.png">
            <p class="iconos-producto-texto">Bluetooth</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/remote-icon.png">
            <p class="iconos-producto-texto">Control Remoto</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/usb-icon.png">
            <p class="iconos-producto-texto">USB</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/disc-icon.png">
            <p class="iconos-producto-texto">CD/MP3</p>
          </div>
          <div class="col-md-2">
            <img class="img-responsive" src="img/assets/radio-icon.png">
            <p class="iconos-producto-texto">FM Digital</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--section id="info-section" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="container info-container-pad">
        <div class="col-md-10 col-md-offset-1">              
          <div class="col-md-4">
            <p class="info-section-title">Maxima Fidelidad y Potencia Inalámbrica</p>
            <p class="info-section-text">
              Con 100W de potencia RMS, escucha tu música con la mayor fidelidad vía  bluetooth y disfruta, sin cables ni conexiones. Empareja fácilmente tus dispositivos como Smartphones o Tablets. 
            </p>
            <p class="info-section-title">Radio FM y lector de CD</p>
            <p class="info-section-text">
              Podrás sintonizar emisoras de FM en forma digital o simplemente escuchar tu CD favorito. 
            </p>
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Conectividad para el Hogar</p>
            <p class="info-section-text">
              Conectá cualquier otro dispositivo como un TV o DVD en la entrada Auxiliar o simplemente usa tu Pen drive o MP3 por USB y controla las pistas desde el control remoto multifunción o el mismo equipo.
            </p>  
            <p class="info-section-title">Cargador de dispositivos</p>
            <p class="info-section-text">
              Carga tu Smartphone o Tablet de manera rápida y sencilla vía USB. 
            </p> 
          </div>              
          <div class="col-md-4">
            <p class="info-section-title">Diseño minimalista</p>
            <p class="info-section-text">
              Con su reducido tamaño, este sistema de audio hogareño se adapta y armoniza con cualquier espacio de la casa.
            </p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section-->

<section id="pantalla-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-size-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Bluetooth</p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-7">
            <p class="interna-cintillo-right-text"><span>Maxima Fidelidad y Potencia Inalámbrica</span><br>Con 100W de potencia RMS, escucha tu música con la mayor fidelidad vía  bluetooth y disfruta, sin cables ni conexiones. Empareja fácilmente tus dispositivos como Smartphones o Tablets.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-03.png">             
      </div>
    </div>
  </div>
</section>

<section id="camara-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cam-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-black">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title-white">Radio FM y lector de <br><span>CD</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line-white"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text-white">Podrás sintonizar emisoras de FM en forma digital o simplemente escuchar tu CD favorito.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/axis/phone-04.png">             
      </div>
    </div>
  </div>
</section>

<section id="hd-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/pantalla-hd-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-grey">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Control<br>Remoto</p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text"><span>Conectividad para el Hogar</span><br>Conectá cualquier otro dispositivo como un TV o DVD en la entrada Auxiliar o simplemente usa tu Pen drive o MP3 por USB y controla las pistas desde el control remoto multifunción o el mismo equipo.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-05.png">             
      </div>
    </div>
  </div>
</section>

<section id="cell-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/cell-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title"><span>USB</span></p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-5">
            <p class="interna-cintillo-right-text"><span>Cargador de dispositivos</span><br>Carga tu Smartphone o Tablet de manera rápida y sencilla vía USB.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad">
        <img class="img-responsive image-pad" src="img/axis/phone-06.png">             
      </div>
    </div>
  </div>
</section>

<section id="sim-axis" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="internas-cintillo">
        <div class="col-md-4 interna-cintillo-left">
          <img class="section-logo" src="img/assets/dual-sim-section.png">
        </div>
        <div class="col-md-8 interna-cintillo-right-white">
          <div align="center" class="col-md-2">
            <p class="interna-cintillo-right-title">Diseño minimalista</p>                
          </div>
          <div class="col-md-1">
            <span class="vertical-line"></span>
          </div>
          <div class="col-md-6">
            <p class="interna-cintillo-right-text">Con su reducido tamaño, este sistema de audio hogareño se adapta y armoniza con cualquier espacio de la casa.</p>
          </div>
        </div>
      </div>
      <div align="center" class="col-md-12 image-pad-no-bottom">
        <img class="img-responsive image-pad-no-bottom" src="img/axis/phone-07.png">             
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>