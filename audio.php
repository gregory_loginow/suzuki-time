<?php include 'assets/header.php' ?>

<header id="header-custom" class="header header-two">
  <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>            
          <a class="navbar-brand" href="index.php"><img class="img-responsive" src="img/assets/logo.png" alt=""></a>              
        </div>

        <div class="navbar-collapse collapse" id="kane-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="index.php">Smartphones</a></li>
            <li><a href="tablets.php">Tablets</a></li>
            <li><a class="active-navbar" href="audio.php">Audio</a></li>
            <li><a href="video.php">Video</a></li>
            <li><a href="car-audio.php">Car Audio</a></li>
            <li><a href="empresa.php">Empresa</a></li>
            <li><a href="contactos.php">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid header-subclass is-visible" data-nav-status="toggle">
    <div class="row">
      <div class="container">
        <div class="col-md-10 col-md-offset-2 kill-padding">
          <h6>AUDIO</h6>
          <ul class="subclass-list">
            <li><a class="subclass-active" href="#ms-250m">MS-250M</a></li>
            <li><a href="#ms-200">MS-200</a></li>
            <li><a href="#ms-202s">MS-200S</a></li>
            <li><a href="#mc-210">MC-210</a></li>
            <li><a href="#sp-130">SP-130</a></li>
            <li><a href="#bb-82l">BB-82L</a></li>
            <li><a href="#bts-05wc">BTS-05WC</a></li>
            <li><a href="#bts-20wr">BTS-20WR</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="ms-250m" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo first-container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MINISISTEMA</h2>
            <p class="equipo-modelo">ms-250m</p><a class="ver-mas-btn" href="ms-250m.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive ms250m-audio-home" src="img/ms250m/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">CD/MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="ms-200" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MINISISTEMA</h2>
            <p class="equipo-modelo">ms-200</p><a class="ver-mas-btn" href="ms-200.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive ms200-audio-home" src="img/ms200/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">CD/MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="ms-202s" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MINISISTEMA</h2>
            <p class="equipo-modelo">ms-202s</p><a class="ver-mas-btn" href="ms-202s.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive ms202s-audio-home" src="img/ms202s/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">CD/MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="mc-210" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">MICROSISTEMA</h2>
            <p class="equipo-modelo">mc-210</p><a class="ver-mas-btn" href="mc-210.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive mc210-audio-home" src="img/mc210/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">CD/MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="sp-130" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">TORRE PARLANTE</h2>
            <p class="equipo-modelo">sp-130</p><a class="ver-mas-btn" href="sp-130.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive sp130-audio-home" src="img/sp130/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/guitar-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Guitarra</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB<br>•Rec</p>
              </th>
              <th>
                <p class="texto-info-cintillo">MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="bb-82l" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">BOOMBOX PORTATIL</h2>
            <p class="equipo-modelo">bb-82l</p><a class="ver-mas-btn" href="bb-82l.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive bb82l-audio-home" src="img/bb82l/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/disc-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="bts-05wc" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">SPEAKER BLUETOOTH</h2>
            <p class="equipo-modelo">bts-05wc</p><a class="ver-mas-btn" href="bts-05wc.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive bts05wc-audio-home" src="img/bts05wc/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="bts-20wr" class="container-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="producto-cintillo container-pad">
        <div class="container">      
          <div class="col-md-8 col-md-offset-2 kill-padding">
            <h2 class="equipo-type">SPEAKER BLUETOOTH</h2>
            <p class="equipo-modelo">bts-20wr</p><a class="ver-mas-btn" href="bts-20wr.php"><i class="fa fa-caret-right"></i> Ver más</a>                
          </div>        
        </div>
      </div>
    </div>
  </div>
  <img class="img-responsive bts20wr-audio-home" src="img/bts20wr/audio-01.png">  
  <div class="container-fluid info-audio-cintillo">
    <div class="row">              
      <div class="container cintillo-audio-pad">
        <div align="center" class="col-md-10 col-md-offset-1 kill-padding">
          <table class="bullets-productos">
            <tr>
              <th>
                <img src="img/assets/bluetooth-icon.png">
              </th>
              <th>
                <img src="img/assets/remote-icon.png">
              </th>
              <th>
                <img src="img/assets/usb-icon.png">
              </th>
              <th>
                <img src="img/assets/mp3-icon.png">
              </th>
              <th>
                <img src="img/assets/radio-icon.png">
              </th>
            </tr>
            <tr>
              <th>
                <p class="texto-info-cintillo">Bluetooth</p>
              </th>
              <th>
                <p class="texto-info-cintillo">Control Remoto</p>
              </th>
              <th>
                <p class="texto-info-cintillo">USB</p>
              </th>
              <th>
                <p class="texto-info-cintillo">MP3</p>
              </th>
              <th>
                <p class="texto-info-cintillo">FM Digital</p>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'assets/footer.php' ?>    